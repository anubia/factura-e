# FACTURAe

## Fuentes de información

- Hilo del grupo de localización española de Odoo (openerp-spain): 
[https://groups.google.com/forum/?hl=es&fromgroups=#!topic/openerp-spain/lOhciA0rlpQ](https://groups.google.com/forum/?hl=es&fromgroups=#!topic/openerp-spain/lOhciA0rlpQ)
  - A través de este grupo e hilo se está intentando coordinar los esfuerzos de desarrollo.
- Página oficial de FACTURAe: [https://face.gob.es/es/](https://face.gob.es/es/)
- Especificaciones técnicas, del Ministerio: [http://www.facturae.gob.es/formato/Paginas/version-3-2.aspx](http://www.facturae.gob.es/formato/Paginas/version-3-2.aspx)
- Utilidades: [http://www.facturae.gob.es/formato/Paginas/utilidades-online.aspx](http://www.facturae.gob.es/formato/Paginas/utilidades-online.aspx)
- Entrada en vigor: 2015.01.15

## Necesidades
- Formato 3.2 o 3.2.1 de FACTURAe
- Firmado electrónicamente


## Firma electrónica
- @Firma (Java)
- Sinadura: [http://www.sinadura.net/es/wik/-/wiki/sinadura/UserManual330#section-UserManual330-SinaduraPorConsola](http://www.sinadura.net/es/wik/-/wiki/sinadura/UserManual330#section-UserManual330-SinaduraPorConsola)


## Repos WIP disponibles
- Localización, obsoleto: [https://github.com/OCA/l10n-spain/tree/6.1/l10n_es_facturae](https://github.com/OCA/l10n-spain/tree/6.1/l10n_es_facturae)
- Pexego: [https://github.com/Pexego/l10n-spain-facturae60_61](https://github.com/Pexego/l10n-spain-facturae60_61)
- TECON (Álvaro Fernández): [https://github.com/TeconSoluciones/l10n_es_facturae.git](https://github.com/TeconSoluciones/l10n_es_facturae.git)
  - >Hecho para la versión 6.0 de OpenERP y XML 3.1 para la Factura-e. Está preparado para firmar también el XML. Para ello, en la ficha de la compañía hay que añadir el certificado para firmar, y la contraseña de ese certificado.
    >
    >El viernes a última hora lo monté para la 6.1 también. Esta mañana lo subiré al repositorio.


## Comentarios
- La v3.2 Se basa en lo que había, por lo tanto, sigue exportando en formato 3.1, sólo he cambiado el formato de exportación para añadirle la posibilidad de exportar con los códigos DIR3 del "Directorio Común de Unidades Orgánicas y Oficinas DIR3", para poder subir el facturae a FACe y que esta plataforma lo remita a la organización correspondiente. Si la  dirección fiscal de la factura no tiene el código dir3 establecido, continuará creando el formato anterior, que es válido pero no para FACe, https://face.gob.es/es/
- Se pueden probar las validaciones en: [http://sedeaplicaciones2.minetur.gob.es/FacturaE/index.jsp](http://sedeaplicaciones2.minetur.gob.es/FacturaE/index.jsp)
- Se pueden convertir al formato 3.2 en [http://sedeaplicaciones2.minetur.gob.es/FacturaECon/](http://sedeaplicaciones2.minetur.gob.es/FacturaECon/) por ejemplo, - Se pueden firmar con el programa que ofrece la página oficial: [http://www.facturae.gob.es/formato/Paginas/descarga-aplicaciones.aspx](http://www.facturae.gob.es/formato/Paginas/descarga-aplicaciones.aspx)

